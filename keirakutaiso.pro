TEMPLATE = app

VERSION = 0.1.0 # major.minor.patch

# https://doc.qt.io/qt-5/qmediametadata.html
# https://doc.qt.io/qt-5/qtlocation-index.html
# https://doc.qt.io/qt-5/qtquickcontrols2-index.html
QT += core \
  qml \
  quick \
  quickcontrols2 \
  widgets \
  multimedia \
  positioning \
  location \
  sql

CONFIG += c++11

# https://doc.qt.io/qt-5/plugins-howto.html#static-plugins
QTPLUGIN += qjpeg

SOURCES += \
  src/imageobject.cpp \
  src/imagelist.cpp \
  src/main.cpp

HEADERS += \
  src/imageobject.h \
  src/imagelist.h

RESOURCES += src/qml.qrc

# Setting the Application Icon on Common Linux Desktops
# https://doc.qt.io/qt-5/appicon.html
# https://www.freedesktop.org/wiki/Specifications/icon-theme-spec/

# Setting the Application Icon on OS X
ICON = src/graphics/nanbu-1969s.icns

# Setting the Application Icon on Windows
RC_ICONS = src/graphics/nanbu-1969s.ico

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

#QMAKE_CC =
#QMAKE_CXX =

# Default rules for deployment.
include(src/deployment.pri)

# Way for the compiler to look for 3rd party stuff
LIBS += -L/usr/lib -L/usr/local/lib -lexiv2
INCLUDEPATH += /usr/local/include
