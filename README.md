# keirakutaiso (経絡体操)

> Qt Quick based desktop application for adding and modifying geotags in image files

Please note that the minimum [Qt](http://www.qt.io/developers/) version supported is `5.12.0` (LTS),
which introduced [the version `2.4` of Qt Quick Controls](http://doc.qt.io/qt-5/qtquickcontrols-index.html#versions), and version `2.12` of Qt Quick.


## Background for the project name

The name `keirakutaiso` (経絡体操) comes from a [Japanese martial art called Nanbudo](https://nanbudo.fi), in which a set of techniques is called by that name.
Those techniques are for stimulating the vital energy points located in the human body, usually used for healthcare treatment in the Asian medicine.

The name can be split into two parts, `keiraku` (経絡) and `taiso` (体操). The latter stands for physical exercise, while the prior stands for meridian paths.

## What this desktop application does?

Lists images of the selected directory in a grid and shows a map next to them.
In case images have geo location information stored in them, those locations will be shown on the map.
Images can be given a location by dragging them on top of the map.

All images opened via this application, will have their `SHA-256` hash stored locally, in order to
quickly see if there have been changes to the file or if there are any duplicates.
At the same time, also the locations and tags used in the images are stored and used as hot spots,
in order to allow setting them faster for any new images.

## Getting started with building sources

On Mac OS X, please note the version numbers in the paths to match the ones installed:

```sh
brew install qt5 gcc make exiv2
/usr/local/Cellar/qt/5.14.1/bin/qmake keirakutaiso.pro
/usr/local/Cellar/make/4.3/bin/gmake
```

Then run via command line to get any console messages:

```sh
keirakutaiso.app/Contents/MacOS/keirakutaiso
```

[How about handling the image meta data?](http://www.exiv2.org/)

Ubuntu:

```sh
sudo apt-get install exiv2 libexiv2-dev
```

macOS with Homebrew:

```sh
brew install exiv2
```

## TODO

* [qmlmin](https://conf.kde.org/system/attachments/31/original/QtQuickTooling.pdf)
* [Mapbox GL - Qt SDK](https://github.com/mapbox/mapbox-gl-native/tree/master/platform/qt)
* [ToolTip](https://doc.qt.io/qt-5.7/qml-qtquick-controls2-tooltip.html)
* [Qt Lite (coming in 5.8), as it might be able to reduce the size of a statically linked Qt Quick application by up to 70% compared to Qt 5.6](http://blog.qt.io/blog/2016/08/18/introducing-the-qt-lite-project-qt-for-any-platform-any-thing-any-size/)
* Auto tagging the country name if GPS location is already defined
* Read all existing tags and create database of them
* Read all existing locations and have the most used as hot spots in the map
* Use a separate thread to read and write metadata
* [Offline map data](http://blog.qt.io/blog/2017/05/24/qtlocation-using-offline-map-tiles-openstreetmap-plugin/)

## Testing

Automated builds via:

* GitLab CI, using [the `frostasm/qt:qt5.12-desktop` Docker image](https://hub.docker.com/r/icsinc/qt5.7.0-x64/)
* AppVeyor, running [Windows Server 2012 R2 (x64) and Qt 5.12.6](https://www.appveyor.com/docs/windows-images-software/#qt), available at `C:\Qt\5.12.6\msvc2015_64`

Linting with [qmllint](http://www.kdab.com/kdab-contributions-qt-5-4-qmllint/):

```sh
find src/qml -maxdepth 2 -type f -name '*.qml' \
 -exec sh -c 'echo "{}" && /usr/local/Cellar/qt5/5.14.1/bin/qmllint "{}"' ';'
```

## Version history

Nothing yet...

## License

Copyright (c) [Juga Paazmaya](https://paazmaya.fi) <paazmaya@yahoo.com>

Licensed under [the MIT license](LICENSE).
