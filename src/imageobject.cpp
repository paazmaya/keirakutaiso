#include <QDebug>
#include <QCryptographicHash>
#include "imageobject.h"

ImageObject::ImageObject(QObject *parent)
    : QObject(parent)
{
}

ImageObject::ImageObject(const QFileInfo &info, QObject *parent)
    : QObject(parent), m_selected(false), m_creator("")
{
    m_name = info.fileName();
    m_path = info.absoluteFilePath();
    m_modified = info.lastModified();
    m_size = qRound((double)info.size() / 1024);
    m_coordinate = QGeoCoordinate(0, 0);

    createHash();
}

/**
 * @see https://doc.qt.io/qt-5/qgeocoordinate.html
 */
void ImageObject::setCoordinate(const double &latitude, const double &longitude)
{
    if (m_coordinate.latitude() != latitude || m_coordinate.longitude() != longitude) {
        m_coordinate = QGeoCoordinate(latitude, longitude);
        emit coordinateChanged();
    }
}

QString ImageObject::getExif(Exiv2::ExifData &exifData, std::string exifKey)
{
    Exiv2::ExifKey key(exifKey);

    Exiv2::ExifData::iterator pos = exifData.findKey(key);
    if (pos == exifData.end()) {
      return "";
    }

    Exiv2::Value::AutoPtr tag = pos->getValue();
    //std::cout << "key" << tag.key() << tag.value();
    return QString::fromStdString(tag->toString());
}

// http://www.exiv2.org/doc/classExiv2_1_1ExifData.html
void ImageObject::readMetaExif(Exiv2::ExifData &exifData)
{

    if (exifData.empty()) {
        qDebug() << "no exif data found via exiv2";
    }
    else {

      qDebug() << "exifData.count()" << exifData.count();

      //qDebug() << "Exif.GPSInfo" << getExif(exifData, "Exif.GPSInfo");
      qDebug() << "Exif.Image.Artist" << getExif(exifData, "Exif.Image.Artist");
      qDebug() << "Exif.Photo.DateTimeOriginal" << getExif(exifData, "Exif.Photo.DateTimeOriginal");

      // Exif.GPSInfo
      // Exif.Image.Artist
      // Exif.Photo.DateTimeOriginal

      /*
      Exiv2::ExifData::const_iterator end = exifData.end();
      for (Exiv2::ExifData::const_iterator i = exifData.begin(); i != end; ++i) {
          //const char* tn = i->typeName();


          std::cout << std::setw(44) << std::setfill(' ') << std::left
                    << i->key() << " "
                    //<< "0x" << std::setw(4) << std::setfill('0') << std::right
                    //<< std::hex << i->tag() << " "
                    //<< std::setw(9) << std::setfill(' ') << std::left
                    //<< (tn ? tn : "Unknown") << " "
                    // << std::dec << std::setw(3)
                    // << std::setfill(' ') << std::right
                    //<< i->count() << "  "
                    << std::dec << i->value()
                    << "\n";
      }
      */
    }
}

void ImageObject::readMetaIptc(Exiv2::IptcData &iptcData)
{

    if (iptcData.empty()) {
        qDebug() << "no iptc data found via exiv2";
    }
    else {
      qDebug() << "iptcData.count()" << iptcData.count();

      Exiv2::IptcData::iterator end = iptcData.end();
      for (Exiv2::IptcData::iterator md = iptcData.begin(); md != end; ++md) {
          std::cout << std::setw(44) << std::setfill(' ') << std::left
                    << md->key() << " "
                    << "0x" << std::setw(4) << std::setfill('0') << std::right
                    << std::hex << md->tag() << " "
                    << std::setw(9) << std::setfill(' ') << std::left
                    << md->typeName() << " "
                    << std::dec << std::setw(3)
                    << std::setfill(' ') << std::right
                    << md->count() << "  "
                    << std::dec << md->value()
                    << std::endl;
      }
    }
}

void ImageObject::readMetaXmp(Exiv2::XmpData &xmpData)
{

    if (xmpData.empty()) {
        qDebug() << "no xmp data found via exiv2";
    }
    else {
      qDebug() << "xmpData.count()" << xmpData.count();

      Exiv2::XmpData::iterator end = xmpData.end();
      for (Exiv2::XmpData::iterator md = xmpData.begin(); md != end; ++md) {
          std::cout << std::setw(44) << std::setfill(' ') << std::left
                    << md->key() << " "
                    << "0x" << std::setw(4) << std::setfill('0') << std::right
                    << std::hex << md->tag() << " "
                    << std::setw(9) << std::setfill(' ') << std::left
                    << md->typeName() << " "
                    << std::dec << std::setw(3)
                    << std::setfill(' ') << std::right
                    << md->count() << "  "
                    << std::dec << md->value()
                    << std::endl;
      }
    }
}


void ImageObject::readMeta()
{
    qDebug() << "reading meta via exiv2 " << m_path;

    // http://www.exiv2.org/doc/classExiv2_1_1ImageFactory.html#aba929c4ca4a71625d12bcb97bcc28161
    Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(m_path.toStdString());

    // Can it read and write exit?
    // http://www.exiv2.org/doc/namespaceExiv2.html#af5f0ea6c944c786f7c4c4841fa22b16c
    int mode = image->checkMode(Exiv2::MetadataId::mdExif);
    qDebug() << "exif data access mode" << mode;


    // http://www.exiv2.org/doc/classExiv2_1_1Image.html#a198b8d5924d6441748aa162130c96a5f
    image->readMetadata();

    readMetaExif(image->exifData());
    //readMetaIptc(image->iptcData());
    //readMetaXmp(image->xmpData());
}

bool ImageObject::writeMeta()
{
    // setExifData (const ExifData &exifData);
    // writeMetadata();

    // image->setExifData(exifData);
    // image->writeMetadata();
    return false;
}

void ImageObject::createHash()
{
  QCryptographicHash crypt(QCryptographicHash::Sha256);
  QFile file(m_path);
  crypt.addData(&file);
  m_hash = QString(crypt.result().toHex());
  crypt.reset();
}
