#ifndef IMAGELIST_H
#define IMAGELIST_H

#include "imageobject.h"
#include <QObject>
#include <QList>
#include <QFileInfo>
#include <QQmlListProperty>
#include <QDir>
#include <QQuickItem>

class ImageObject;


class ImageList : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QString directory READ directory WRITE setDirectory NOTIFY directoryChanged)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)

    Q_PROPERTY(QQmlListProperty<ImageObject> list READ list NOTIFY listChanged)
    Q_PROPERTY(int length READ length NOTIFY listChanged)

    // used for setting children as properties of list
    Q_CLASSINFO("DefaultProperty", "list")

public:
    ImageList(QQuickItem *parent = 0);

    QString directory() const
    {
        return m_dir.absolutePath();
    }
    void setDirectory(const QString &directory);

    QString filter() const
    {
        return m_filter;
    }
    void setFilter(const QString &filter)
    {
        if (filter != m_filter) {
            m_filter = filter;
            m_dir.setNameFilters(filter.split(','));
            emit filterChanged();
        }
    }

    // used for setting children as properties of list
    QQmlListProperty<ImageObject> list();

    int length() const
    {
        return m_list.count();
    }

    Q_INVOKABLE ImageObject *image(int) const;

signals:
    void directoryChanged();
    void filterChanged();
    void listChanged();

private:
    static void appendImage(QQmlListProperty<ImageObject> *list, ImageObject *img);
    static int imageCount(QQmlListProperty<ImageObject>*list);
    static ImageObject* imageAt(QQmlListProperty<ImageObject> *list, int i);
    static void imageClear(QQmlListProperty<ImageObject> *list);

    QString m_filter;
    QList<ImageObject*> m_list;
    QDir m_dir;
};

#endif // IMAGELIST_H
