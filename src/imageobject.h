#ifndef IMAGEOBJECT_H
#define IMAGEOBJECT_H

#include <QObject>
#include <QDateTime>
#include <QFileInfo>
#include <QGeoCoordinate>
#include <QSize>
#include <QImage>

#include <iostream>

#include <exiv2/exiv2.hpp>

class ImageList;

class ImageObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(QString path READ path NOTIFY pathChanged)
    Q_PROPERTY(QString modified READ modified NOTIFY modifiedChanged)
    Q_PROPERTY(int size READ size NOTIFY sizeChanged)
    Q_PROPERTY(QSize resolution READ resolution NOTIFY resolutionChanged)

    // https://doc.qt.io/qt-5/qml-coordinate.html
    Q_PROPERTY(QGeoCoordinate coordinate READ coordinate NOTIFY coordinateChanged)

    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)
    Q_PROPERTY(QString creator READ creator WRITE setCreator NOTIFY creatorChanged)
    Q_PROPERTY(QString hash READ hash NOTIFY hashChanged)

public:
    ImageObject(QObject *parent = 0);
    ImageObject(const QFileInfo &info, QObject *parent = 0);

    QString name() const
    {
        return m_name;
    }
    QString path() const
    {
        return m_path;
    }
    QString modified() const
    {
        return m_modified.toString("yyyy-MM-dd HH:mm");
    }
    int size() const
    {
        return m_size;
    }
    QSize resolution() const
    {
        return m_resolution;
    }
    QGeoCoordinate coordinate() const
    {
        return m_coordinate;
    }

    bool selected() const
    {
        return m_selected;
    }
    void setSelected(const bool &selected)
    {
        if (selected != m_selected) {
            m_selected = selected;
            emit selectedChanged();
        }
    }

    QString creator() const
    {
        return m_creator;
    }
    void setCreator(const QString &creator)
    {
        if (creator != m_creator) {
            m_creator = creator;
            emit creatorChanged();
        }
    }

    QString hash() const
    {
        return m_hash;
    }

public slots:
    void setCoordinate(const double &latitude, const double &longitude);
    void readMeta();
    bool writeMeta();

signals:
    void nameChanged();
    void pathChanged();
    void modifiedChanged();
    void sizeChanged();
    void resolutionChanged();
    void coordinateChanged();

    void selectedChanged();
    void creatorChanged();
    void hashChanged();

private:
    QString m_name;
    QString m_path;
    QDateTime m_modified;
    int m_size; // in KiloBytes
    QSize m_resolution;

    bool m_selected;
    QString m_creator;
    QGeoCoordinate m_coordinate;
    QString m_hash; // SHA-256

    QString getExif(Exiv2::ExifData &exifData, std::string exifKey);
    void readMetaExif(Exiv2::ExifData &exifData);
    void readMetaIptc(Exiv2::IptcData &iptcData);
    void readMetaXmp(Exiv2::XmpData &xmpData);

    void createHash();
};

// Q_DECLARE_METATYPE(QQmlListProperty<ImageObject>)

#endif // IMAGEOBJECT_H
