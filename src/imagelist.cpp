#include <QDebug>
#include <QUrl>
#include <QQmlEngine>
#include "imagelist.h"

ImageList::ImageList(QQuickItem *parent) : QQuickItem(parent),
  m_filter(QString("*.png,*.jpeg,*.jpg,*.gif,*.bmp")),
  m_dir(QDir::home())
{
  QList<ImageObject*> m_list;

  qDebug() << m_filter << "are my filters";
  m_dir.setNameFilters(m_filter.split(','));
}

/**
 * Please note that the directory contains the file:// scheme
 */
void ImageList::setDirectory(const QString &directory)
{
    QUrl url(directory);
    QString localDir(url.toLocalFile());

    if (localDir != m_dir.absolutePath()) {

        // Should read the directory and update list
        m_dir.setPath(localDir);
        emit directoryChanged();

        QFileInfoList list = m_dir.entryInfoList(
          QDir::Files | QDir::Readable | QDir::NoDotAndDotDot,
          QDir::Name | QDir::DirsFirst
        );

        qDebug() << "list.size()" << list.size();

        m_list.clear();
        qDebug() << "m_list.size() before:" << m_list.size();


        for (int i=0; i < list.count(); ++i) {
            QFileInfo info = list.at(i);
            if (info.isFile()) {
                ImageObject* entry = new ImageObject(info);
                m_list << entry;
            }
        }

        emit listChanged();
    }
}

QQmlListProperty<ImageObject> ImageList::list()
{
    qDebug() << "reading list";

    return QQmlListProperty<ImageObject>(this, 0,
      &ImageList::appendImage, &ImageList::imageCount, &ImageList::imageAt, &ImageList::imageClear
    );

}

ImageObject *ImageList::image(int index) const
{
    return m_list.at(index);
}


void ImageList::appendImage(QQmlListProperty<ImageObject> *list, ImageObject *img)
{
    qDebug() << "should be appending";

    ImageList *imgList = qobject_cast<ImageList *>(list->object);
    if (imgList && img) {
        imgList->m_list.append(img);
        qDebug() << "appendImage is gonna emit. imgList->m_list.size()" << imgList->m_list.size();
        emit imgList->listChanged();
    }
}

int ImageList::imageCount(QQmlListProperty<ImageObject> *list)
{
    ImageList *imgList = qobject_cast<ImageList*>(list->object);
    if (imgList) {
        return imgList->m_list.count();
    }
    return 0;
}

ImageObject *ImageList::imageAt(QQmlListProperty<ImageObject> *list, int i)
{
    ImageList *imgList = qobject_cast<ImageList*>(list->object);
    if (imgList) {
        return imgList->m_list.at(i);
    }
    qDebug() << "should be getting at, but going to return 0";
    return 0;
}

void ImageList::imageClear(QQmlListProperty<ImageObject> *list)
{
    qDebug() << "should be clearing";
    ImageList *imgList = qobject_cast<ImageList*>(list->object);
    if (imgList) {
        imgList->m_list.clear();
        emit imgList->listChanged();
    }
}
