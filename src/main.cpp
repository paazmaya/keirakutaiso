
// #include <QMapbox>

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQuickControls2>
#include <QtQml>
#include "imageobject.h"
#include "imagelist.h"

#include <QQmlEngine>
#include <qqmlcontext.h>

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlError>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("paazmaya");
    QCoreApplication::setOrganizationDomain("paazmaya.fi");
    QCoreApplication::setApplicationName("keirakutaiso");
    QCoreApplication::setApplicationVersion("0.1.0");

    QApplication app(argc, argv);

    qmlRegisterType<ImageObject>("Keiraku", 1, 0, "ImageObject");
    qmlRegisterType<ImageList>("Keiraku", 1, 0, "ImageList");

    // Exposes the QQuickMapboxGL module so we
    // can do `import QQuickMapboxGL 1.0`.
    //QMapbox::registerTypes();


    QQmlApplicationEngine engine;
    // http://doc.qt.io/qt-5/qqmlengine.html#offlineStoragePath-prop
    qDebug() << "engine.offlineStoragePath()" << engine.offlineStoragePath();

    QString dbName(QCoreApplication::applicationName() + "-v" + QCoreApplication::applicationVersion() + ".sqlite");
    qDebug() << "dbName" << dbName;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbName);
    bool ok = db.open();
    qDebug() << "database open" << ok;
    QSqlQuery sql(db);

    // First enable foreign key support
    sql.exec("PRAGMA foreign_keys = ON;");
    if (sql.lastError().type() != QSqlError::NoError) {
      qDebug() << "foreign_keys error" << sql.lastError().text();
    }

    // https://sqlite.org/lang_createtable.html
    // https://sqlite.org/datatype3.html
    sql.exec("CREATE TABLE IF NOT EXISTS files"
      "("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "filepath TEXT UNIQUE ON CONFLICT REPLACE,"
        "latitude REAL,"
        "longitude REAL,"
        "sha256 TEXT,"
        //"taken INTEGER," // Exif.Photo.DateTimeOriginal
        "modified TEXT DEFAULT CURRENT_TIMESTAMP" // UTC "YYYY-MM-DD HH:MM:SS"
      ");"
    );
    if (sql.lastError().type() != QSqlError::NoError) {
      qDebug() << "files error" << sql.lastError().text();
    }

    sql.exec("CREATE TABLE IF NOT EXISTS tags"
      "("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "name TEXT UNIQUE ON CONFLICT IGNORE,"
        "modified TEXT DEFAULT CURRENT_TIMESTAMP" // UTC "YYYY-MM-DD HH:MM:SS"
      ");"
    );
    if (sql.lastError().type() != QSqlError::NoError) {
      qDebug() << "tags error" << sql.lastError().text();
    }

    // Connections between those two
    sql.exec("CREATE TABLE IF NOT EXISTS tag_file"
      "("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "id_file INTEGER," // id in the files table
        "id_tag INTEGER," // id in the tags table
        "modified TEXT DEFAULT CURRENT_TIMESTAMP," // UTC "YYYY-MM-DD HH:MM:SS"
        "FOREIGN KEY(id_file) REFERENCES files(id),"
        "FOREIGN KEY(id_tag) REFERENCES tags(id)"
      ");"
    );
    if (sql.lastError().type() != QSqlError::NoError) {
      qDebug() << "tag_file error" << sql.lastError().text();
    }

    qDebug() << "tables:" << db.tables();


    QQmlContext *context = engine.rootContext();
    context->setContextProperty("ApplicationVersion", QCoreApplication::applicationVersion());

    // context->setContextProperty("ImageListModel", QVariant::fromValue(dataList));

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
