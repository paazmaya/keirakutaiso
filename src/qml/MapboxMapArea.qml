import QtQuick 2.12

import QtQuick.Controls 2.12

import QtGraphicalEffects 1.0
import QtPositioning 5.0
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.0

import QQuickMapboxGL 1.0

Rectangle {
  MapboxMap {
      id: map
      anchors.fill: parent

      parameters: [
          MapParameter {
              id: style
              property var type: "style"
              property var url: "mapbox://styles/mapbox/streets-v9"
          },
          MapParameter {
              id: waterPaint
              property var type: "paint"
              property var layer: "water"
              property var fillColor: waterColorDialog.color
          },
          MapParameter {
              property var type: "source"
              property var name: "routeSource"
              property var sourceType: "geojson"
              property var data: ":source.geojson"
          },
          MapParameter {
              property var type: "layer"
              property var name: "routeCase"
              property var layerType: "line"
              property var source: "routeSource"
          },
          MapParameter {
              property var type: "paint"
              property var layer: "routeCase"
              property var lineColor: "white"
              property var lineWidth: 20.0
          },
          MapParameter {
              property var type: "layout"
              property var layer: "routeCase"
              property var lineJoin: "round"
              property var lineCap: lineJoin
              property var visibility: toggleRoute.checked ? "visible" : "none"
          },
          MapParameter {
              property var type: "layer"
              property var name: "route"
              property var layerType: "line"
              property var source: "routeSource"
          },
          MapParameter {
              id: linePaint
              property var type: "paint"
              property var layer: "route"
              property var lineColor: "blue"
              property var lineWidth: 8.0
          },
          MapParameter {
              property var type: "layout"
              property var layer: "route"
              property var lineJoin: "round"
              property var lineCap: "round"
              property var visibility: toggleRoute.checked ? "visible" : "none"
          },
          MapParameter {
              property var type: "image"
              property var name: "label-arrow"
              property var sprite: ":label-arrow.svg"
          },
          MapParameter {
              property var type: "image"
              property var name: "label-background"
              property var sprite: ":label-background.svg"
          },
          MapParameter {
              property var type: "layer"
              property var name: "markerArrow"
              property var layerType: "symbol"
              property var source: "routeSource"
          },
          MapParameter {
              property var type: "layout"
              property var layer: "markerArrow"
              property var iconImage: "label-arrow"
              property var iconSize: 0.5
              property var iconIgnorePlacement: true
              property var iconOffset: [ 0.0, -15.0 ]
              property var visibility: toggleRoute.checked ? "visible" : "none"
          },
          MapParameter {
              property var type: "layer"
              property var name: "markerBackground"
              property var layerType: "symbol"
              property var source: "routeSource"
          },
          MapParameter {
              property var type: "layout"
              property var layer: "markerBackground"
              property var iconImage: "label-background"
              property var textField: "{name}"
              property var iconTextFit: "both"
              property var iconIgnorePlacement: true
              property var textIgnorePlacement: true
              property var textAnchor: "left"
              property var textSize: 16.0
              property var textPadding: 0.0
              property var textLineHeight: 1.0
              property var textMaxWidth: 8.0
              property var iconTextFitPadding: [ 15.0, 10.0, 15.0, 10.0 ]
              property var textOffset: [ -0.5, -1.5 ]
              property var visibility: toggleRoute.checked ? "visible" : "none"
          },
          MapParameter {
              property var type: "paint"
              property var layer: "markerBackground"
              property var textColor: "white"
          },
          MapParameter {
              property var type: "filter"
              property var layer: "markerArrow"
              property var filter: [ "==", "$type", "Point" ]
          },
          MapParameter {
              property var type: "filter"
              property var layer: "markerBackground"
              property var filter: [ "==", "$type", "Point" ]
          },
          MapParameter {
              property var type: "bearing"
              property var angle: bearingSlider.value
          },
          MapParameter {
              property var type: "pitch"
              property var angle: pitchSlider.value
          }
      ]

      center: QtPositioning.coordinate(60.170448, 24.942046) // Helsinki
      zoomLevel: 12.25
      minimumZoomLevel: 0
      maximumZoomLevel: 20

      color: landColorDialog.color
      copyrightsVisible: true

      states: State {
          name: "moved"; when: mouseArea.pressed
          PropertyChanges { target: linePaint; lineColor: "red"; }
      }

      transitions: Transition {
          ColorAnimation { properties: "lineColor"; easing.type: Easing.InOutQuad; duration: 500 }
      }

      Image {
          anchors.right: parent.right
          anchors.bottom: parent.bottom
          anchors.margins: 20

          opacity: .75

          sourceSize.width: 80
          sourceSize.height: 80

          source: "icon.png"
      }

      MouseArea {
          id: mouseArea
          anchors.fill: parent

          property var lastX: 0
          property var lastY: 0

          onWheel: map.zoomLevel += 0.2 * wheel.angleDelta.y / 120

          onPressed: {
              lastX = mouse.x
              lastY = mouse.y
          }

          onPositionChanged: {
              map.pan(mouse.x - lastX, mouse.y - lastY)

              lastX = mouse.x
              lastY = mouse.y
          }
      }
  }
}
