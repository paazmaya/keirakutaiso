import QtQuick 2.12

// http://doc.qt.io/qt-5/qml-qtquick-gridview.html
Rectangle {
  property alias model: gridView.model

  onModelChanged: {
    console.log('Model changed at ImageGrid.qml');
  }

  width: 400
  height: 600
  color: "lightblue"

  GridView {
    id: gridView
    anchors.fill: parent
    cellHeight: 200
    cellWidth: 200

    delegate: ImageItem {
      width: gridView.cellWidth
      height: gridView.cellHeight
    }
  }
}
