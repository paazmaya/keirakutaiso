import QtQuick 2.12

// http://doc.qt.io/qt-5/qtquickcontrols2-index.html
import QtQuick.Controls 2.12

// import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    modal: true
    width: 400
    height: 300
    title: "About"
    standardButtons: Dialog.Ok

    header: TabBar {
        TabButton {
            text: qsTr("About")
        }
        TabButton {
            text: qsTr("Included software")
        }
    }

    StackView {
        id: stack
        initialItem: aboutGeneral
        anchors.fill: parent
    }

    Component {
        id: aboutGeneral
        Frame {
            RowLayout {
                anchors.fill: parent
                spacing: 6

                Image {
                    Layout.minimumWidth: 96
                    Layout.preferredWidth: 96
                    Layout.maximumWidth: 96
                    Layout.minimumHeight: 96

                    source: 'qrc:/graphics/96x96.png'
                }

                Column {
                    Layout.fillWidth: true
                    Layout.minimumWidth: 100
                    Layout.preferredWidth: 200
                    Layout.preferredHeight: 100

                    Label {
                      text: Qt.application.name
                    }

                    Label {
                      text: qsTr("Version ") + Qt.application.version
                    }
                }
            }
        }

    }
    onAccepted: console.log("Ok clicked")
}
