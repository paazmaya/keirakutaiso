import QtQuick 2.12

import QtQuick.Controls 2.12

import QtQuick.Dialogs 1.2

import QtQuick.Layouts 1.2

// http://doc.qt.io/qt-5/qml-qt-labs-settings-settings.html
import Qt.labs.settings 1.0

import Keiraku 1.0

// http://doc.qt.io/qt-5/qml-qtquick-layouts-rowlayout.html
RowLayout {
    property alias iGrid: imageGrid
    property alias map: mapView.map
    property var imageList
    spacing: 0

    Settings {
        id: settings
        category: "SplitView"
        property real widthRatio: 0.5
    }

    anchors.fill: parent

    onWidthChanged: {
        settings.widthRatio = imageGrid.width / parent.width;
        imageGrid.width = width * settings.widthRatio
    }

    ImageGrid {
        id: imageGrid
        Layout.minimumWidth: 200
        Layout.fillHeight: true

        Component.onCompleted: {
            console.log(' width ratio', (imageGrid.width / parent.width));
            width = parent.width * settings.widthRatio
        }

        model: imageList.list

        Drag.dragType: Drag.Automatic
        Drag.supportedActions: Qt.CopyAction

        Drag.onDragFinished: {
            console.log('Drag finished', dropAction);
            var coord = mapView.map.toCoordinate(Qt.point(mapMouseArea.mouseX, mapMouseArea.mouseY));
            console.log(coord);
        }

    }

    MouseArea {
        id: dividerArea
        Layout.fillHeight: true
        Layout.preferredWidth: 20
        hoverEnabled: true

        drag.threshold: 0
        drag.target: dividerTarget
        drag.axis: Drag.XAxis
        cursorShape: Qt.SplitHCursor

        Rectangle {
            id: dividerTarget

            anchors.fill: parent
            color: "blue"
            opacity: 0.5
        }

    }

    DropArea {
        id: dragTarget
        Layout.minimumWidth: 200
        Layout.fillWidth: true
        Layout.fillHeight: true

        MouseArea {
            id: mapMouseArea
            anchors.fill: parent
            //drag.target: parent

            MapView {
                id: mapView
                anchors.fill: parent
            }

            Rectangle {
                id: dropRectangle

                anchors.fill: parent
                color: "blue"
                opacity: 0.5

                states: [
                    State {
                        when: dragTarget.containsDrag
                        PropertyChanges {
                            target: dropRectangle
                            color: "grey"
                        }
                    }
                ]
            }

        }
    }
}
