import QtQuick 2.12

Item {

  // http://doc.qt.io/qt-5/qml-qtquick-mousearea.html
  MouseArea {
    id: dragArea
    anchors.fill: parent
    // If enabled, the gid will no longer scroll
    // drag.target: imageGrid
    drag.maximumX: 0
    drag.maximumY: 0

    // SHould have another mouse area for other than dragging
    // drag.filterChildren: true

    onPressed: {
        selected = !selected;
        parent.grabToImage(function(result) {
            imageGrid.Drag.imageSource = result.url
        })
    }
    drag.onActiveChanged: {
        imageGrid.Drag.active = drag.active
    }

    Image {
      anchors.fill: parent
      fillMode: Image.PreserveAspectCrop
      source: 'file://' + path
      anchors.horizontalCenter: parent.horizontalCenter

      // Constrain memory usage
      sourceSize.width: parent.width
      sourceSize.height: parent.height
    }

    Column {
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
      Text {
        text: name
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Text {
        text: size + ' KB'
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Text {
        text: modified
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Text {
        text: hash
        anchors.horizontalCenter: parent.horizontalCenter
      }
    }

    Rectangle {
      anchors.fill: parent
      visible: selected
      color: "lightblue"
      opacity: 0.4
    }
  }

  Component.onCompleted: {
    readMeta();
  }
}
