import QtQuick 2.12

// http://doc.qt.io/qt-5/qtquickcontrols2-index.html
import QtQuick.Controls 2.12

import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

// http://doc.qt.io/qt-5/qml-qt-labs-settings-settings.html
import Qt.labs.settings 1.0

import Keiraku 1.0

ApplicationWindow {
    id: window
    visible: true
    title: Qt.application.name
    width: 640 // Overwritten from settings
    height: 480 // Overwritten from settings

    Settings {
        id: settingsAW
        category: "ApplicationWindow"
        property alias x: window.x
        property alias y: window.y
        property alias width: window.width
        property alias height: window.height
        property alias lastDirectory: imageList.directory
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            Action {
                text: qsTr("&Open directory")
                onTriggered: fileDialog.open()
            }
            MenuSeparator { }
            Action {
                text: qsTr("&Quit")
                onTriggered: Qt.quit();
            }
        }
        Menu {
            title: qsTr("&Edit")
            Action { text: qsTr("Cu&t") }
            Action { text: qsTr("&Copy") }
            Action { text: qsTr("&Paste") }
            MenuSeparator { }
            Menu {
                title: qsTr("&Find/Replace")
                Action { text: qsTr("Find &Next") }
                Action { text: qsTr("Find &Previous") }
                Action { text: qsTr("&Replace...") }
            }
        }
        Menu {
            title: qsTr("&Help")
            Action {
                text: qsTr("&About")
                onTriggered: aboutDialog.open()
            }
        }
    }

    AboutDialog {
        id: aboutDialog
        anchors.centerIn: parent
    }

    // http://doc.qt.io/qt-5/qml-qtquick-dialogs-filedialog.html
    FileDialog {
        id: fileDialog
        title: qsTr("Please choose a folder")
        modality: Qt.WindowModal
        selectFolder: true
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
            imageList.directory = fileDialog.fileUrls.pop()
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: {
            console.log('Initialising FileDialog. imageList.directory: ' + imageList.directory);
            folder = imageList.directory //shortcuts.home
        }
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }

    footer: ToolBar {
        RowLayout {
            anchors.fill: parent
            Label {
                text: imageList.directory
            }
            Label {
                text: spitView.map.center.toString()
            }
        }
    }



    ImageList {
        id: imageList
        onListChanged: {
            console.log('list changed, length: ' + length);
            //spitView.iGrid.model = imageList.list;
        }
    }

    SplittedView {
        id: spitView
        imageList: imageList
    }

}
