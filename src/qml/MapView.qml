import QtQuick 2.12

import QtPositioning 5.5
import QtLocation 5.6

Rectangle {
    property alias map: mapArea

    width: 300
    height: 600
    color: "lightgreen"

    // http://doc.qt.io/qt-5/location-plugin-here.html
    Plugin {
        id: herePlugin
        name: "here"
        PluginParameter {
            name: "here.app_id"
            value: "fA9FS9dNG1vX91JOA3vf"
        }
        PluginParameter {
            name: "here.token"
            value: "ddOxCLAoZKBEnehSHYuxnQ"
        }
        PluginParameter {
            name: "proxy"
            value: "system"
        }
    }

    // http://doc.qt.io/qt-5/location-plugin-osm.html
    Plugin {
        id: osmPlugin
        name: "osm"
        PluginParameter {
            name: "osm.useragent"
            value: "keirakutaiso 0.1.0"
        }
        PluginParameter {
            name: "osm.mapping.host"
            value: "http://osm.tile.server.address/"
        }
        PluginParameter {
            name: "osm.mapping.copyright"
            value: "All mine"
        }
        PluginParameter {
            name: "osm.routing.host"
            value: "http://osrm.server.address/viaroute"
        }
        PluginParameter {
            name: "osm.geocoding.host"
            value: "http://geocoding.server.address"
        }
    }

    // http://doc.qt.io/qt-5/location-plugin-mapbox.html
    Plugin {
        id: mapboxPlugin
        name: "mapbox"
        PluginParameter {
            name: "mapbox.access_token"
            value: "pk.eyJ1IjoicGFhem1heWEiLCJhIjoiY2ltZjBsYWxtMDA0Z3ZpbHVtYmVieW44NiJ9.rR1lazusBFkgPUGzdhLdYw"
        }
        PluginParameter {
            name: "mapbox.map_id"
            value: "paazmaya.pia6pe1o"
        }
        PluginParameter {
            name: "useragent"
            value: "keirakutaiso 0.1.0"
        }
    }

    Plugin {
        id: providerPlugin
        preferred: ["here", "mapbox"]
    }


    Map {
        id: mapArea
        anchors.fill: parent

        plugin: mapboxPlugin
        zoomLevel: (maximumZoomLevel - minimumZoomLevel) * 0.65
        center {
            latitude: 60.22096598199422
            longitude: 24.916352046556028
        }

        onCenterChanged: {
            // console.log('center changed', center.latitude, center.longitude);
        }

        gesture.enabled: true
        gesture.acceptedGestures: MapGestureArea.PinchGesture | MapGestureArea.PanGesture
                                  | MapGestureArea.FlickGesture
        gesture.onPanStarted: {
            console.log('pan started')
        }
        gesture.onPinchUpdated: {
            console.log(event)
        }

        MouseArea {
            onDoubleClicked: mapArea.centerOnPosition()
        }


    }
}
